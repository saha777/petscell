package lab.life.bookmarks.petscell.service;

import lab.life.bookmarks.petscell.model.User;
import lab.life.bookmarks.petscell.repository.UsersRepository;
import org.springframework.aop.target.LazyInitTargetSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsersService {

    @Autowired
    private UsersRepository usersRepository;

    public List<User> getAll() {
        return usersRepository.findAll();
    }

}
