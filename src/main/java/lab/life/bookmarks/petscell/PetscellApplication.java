package lab.life.bookmarks.petscell;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PetscellApplication {

    public static void main(String[] args) {
        SpringApplication.run(PetscellApplication.class, args);
    }
}
