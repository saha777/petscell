package lab.life.bookmarks.petscell.rest;

import lab.life.bookmarks.petscell.model.User;
import lab.life.bookmarks.petscell.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UsersController {

    @Autowired
    private UsersService usersService;

    @GetMapping
    private ResponseEntity<List<User>> getAll() {
        return ResponseEntity.ok(usersService.getAll());
    }
}
