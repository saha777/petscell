package lab.life.bookmarks.petscell.repository;

import lab.life.bookmarks.petscell.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsersRepository extends JpaRepository<User, Long> {

    @Override
    List<User> findAll();

}
