package lab.life.bookmarks.petscell.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

// Lombok
@Data
@NoArgsConstructor
@AllArgsConstructor

// JPA
@Entity
@Table(name = "users")
public class User {

    @Id
    @Generated(GenerationTime.INSERT)
    private Long id;

    @NotBlank
    @Email
    @Size(min = 5, max = 100)
    private String email;

    @JsonIgnore
    private String password;

    @NotBlank
    @Size(min = 4, max = 254)
    private String name;

    @NotBlank
    @Size(min = 4, max = 254)
    private String lastname;

}
